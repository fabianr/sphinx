Design Vision and Principles
============================

.. figure:: /img/HIGDesignVisionFullBleed.png
   :scale: 50 %
   :alt: Different DPIs on desktop and mobile
   
   ''Simple by default, powerful when needed.''


Design is driven at the highest level by a vision. We rely on a set of design principles to fulfill that design vision. These principles guide all aspects of the user interface design.

Design Vision
-------------
The design vision focuses on two attributes of KDE software that connect its past to its future.

* ''Simple by default'' - Simple and inviting. KDE software is pleasant to experience and easy to use.
* ''Powerful when needed'' - Power and flexibility. KDE software allows users to be effortlessly creative and efficiently productive.

Design Principles
-----------------
The following design principles are used in support of the Design Vision.
*Simple by default
** ''Make it easy to focus on what matters.'' - Remove or minimize elements not crucial to the primary or main task. Use spacing to keep things organized. Use color to draw attention. Reveal additional information or optional functions when needed, later in the presentation.
** ''I know how to do that!'' - Make things easier to learn by reusing design patterns from other applications. Other applications that use good design are a precedent to follow.
** ''Do the heavy lifting for me.'' - Make complex tasks simple. Make novices feel like experts. Create ways in which your users can naturally feel empowered by your software.

*Powerful when needed
** ''Solve a problem.'' - Identify and make very clear to the user what need is addressed and how.
** ''Always in control.'' - It should always be clear what can be done, what is happening and what happened. The user should never feel at the mercy of the tool. Give the user the final say.
** ''Be flexible.'' - Provide sensible defaults but consider optional functionality or customizations that do not interfere with the primary task.


{{Prevnext2|nextpage=KDE_Visual_Design_Group/HIG/Concept|nexttext=Concept|index=KDE_Visual_Design_Group/HIG#Getting_Started|indextext=Back to Getting Started}}


<span style="font-size: 85%">Elements of these guidelines have been adapted, per license, from [https://developer.android.com/design/index.html Android Design].</span>
