Style
=====

.. toctree::
   :caption: Contents:
   :titlesonly:
   :hidden:
   
   imagery
   typography
   writing/index
   
The Style layer is concerned with emotion, tone, and visual vocabulary. Because it is the most visible and concrete aspect of an interface, it typically accounts for people’s first impression of a product. Style is influenced by the use of color,the design of icons throughout the interface and the use of typography. Elements of style support Organization, Viewing and Navigation, Editing and Manipulation and User Assistance.

* :doc:`imagery`
* :doc:`typography`
* :doc:`writing/index`
