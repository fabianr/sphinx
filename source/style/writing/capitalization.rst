Capitalization
==============

Purpose
-------

*Capitalization* is a feature of case-sensitive languages to foster
relevance. In terms of software it draws attention to words with capital
letters. For a consistent look and feel of the software it is important
to implement capitalization consistently. On the other hand,
capitalization slows down translation and increases the risk of
inconsistent terminology.

Guidelines
----------

There are two types of capitalization, title capitalization and sentence
style capitalization:

Title Capitalization
~~~~~~~~~~~~~~~~~~~~

Title capitalization is when every word in a sentence, statement,
phrase, or label are capitalized except for certain words. Words with
less than five letters are generally lowercase in titles, unless they
are the first or last words in a title.

Do not capitalize:

-  Articles: a, an, the
-  Coordinating Conjunctions: and, but, or, for, nor, etc.
-  Prepositions (fewer than five letters): on, at, to, from, by, etc.

Always capitalize

-  Nouns (man, bus, book)
-  Adjectives (angry, lovely, small)
-  Verbs (run, eat, sleep)
-  Adverbs (slowly, quickly, quietly)
-  Pronouns (he, she, it)
-  Subordinating conjunctions (as, because, that)

Use title capitalization in the following cases:

-  Window and dialog box titles
-  Group box / group line labels
-  Button labels
-  Tab labels
-  Listview column headers
-  Menu titles / menu items
-  Derivatives of KCommand
-  Combobox items
-  Listbox items
-  Tree list items
-  Other heading/title text

Sentence Style Capitalization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sentence style capitalization is when the first letter of the sentence,
statement, phrase, or label is capitalized and all other words are lower
case. The only exception to this is proper nouns which are always
capitalized.

Use sentence style capitalization in the following cases:

-  Edit box labels
-  List box labels
-  Combo box labels
-  Spin box labels
-  Check box labels
-  Option button labels
-  Slider labels
-  Pop-up hint text
-  Dialog header/description
-  Other non heading/title text

Acronyms/Initialisms, Internet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Words such as URL, JPEG, or LDAP should be written in capital letters
-  Internet (if referring to *the* Internet) takes a capital I.
