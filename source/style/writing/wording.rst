Wording
=======

Purpose
-------

Every word displayed in an application is part of a conversation with
users. This conversation is an opportunity to provide clarity and to
help people feel comfortable in the system.

Guidelines
----------

-  Use terminology that is familiar and easy to understand for the
   target audience (i.e. Persona) of your application.
-  Start your sentence with the most important objective first.
-  Use short words, active verbs and common nouns.
-  Avoid :doc:`contraction` such as "Don't", "Won't", etc.
-  Avoid abbreviations (including ampersand instead of 'and'), acronyms,
   and tech-babble.
-  Use an informal and friendly tone, but not too informal or humorous.
-  Avoid popular, but transient, words and phrases.
-  Keep information short and consistent; avoid redundancy or wordiness.
   e.g. Do not repeat the dialog title in the dialog text.
-  Don't abuse :doc:`capitalization` because it draws people’s attention.
-  For date and time information, consider that your app may potentially
   be used for several years; do not use immutable date references like
   *this year* unless it is algorithmically determined.
-  Follow system-wide wording conventions for basic functions to keep
   wording consistent.
