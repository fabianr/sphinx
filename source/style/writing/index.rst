Writing
=======

.. toctree::
   :caption: Contents:
   :titlesonly:
   :hidden:
   
   wording
   capitalization
   
Text includes all the written, language-based elements of the interface. This includes the labels used to represent the organizational model, the names of the input and navigational controls contained for Viewing and Navigation, and the alert messages and help text used for User Assistance. 

* :doc:`wording`
* :doc:`capitalization`
